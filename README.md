#OmniFocusAnalyticsFramework#

##Usage##

- Initialize the OFManager with an NSString pointing to your database. By default, it lives in *$USER/Library/Caches/com.omnigroup.OmniFocus/OmniFocusDatabase2*

```OFManager *ofm = [[OFManager alloc] initWithDatabase:ofdb];```

- Create task or project filters to pull out what you want are interested in

```
// all tasks

    OFTaskFilter *tf = [[OFTaskFilter alloc] initWithTasks:[[ofm tasks] allValues] andManager:ofm];

// active tasks

        NSArray *activeTasks = [tf filterActiveTasks];
        OFTaskFilter *activeTaskFilter = [[OFTaskFilter alloc] initWithTasks:activeTasks andManager:ofm];

// active, incomplete tasks

        NSArray *activeIncompleteTasks = [activeTaskFilter filterIncompleteTasks];
```

- Count or list what you are interested in:

```
        int count = [activeIncompleteTasks count];
        NSEnumerator ten = [activeIncompleteTasks objectEnumerator];
        OFTask *task;
        while (task = [ten nextObject])
        {
            NSLog(@"Task is %@", [task name]);
        }
```   

##License##

see LICENSE.txt (MIT license)

##Other##

Comments, complaints, suggestions, etc: please send to ofaf@homeforderangedscientists.net

##Thanks##