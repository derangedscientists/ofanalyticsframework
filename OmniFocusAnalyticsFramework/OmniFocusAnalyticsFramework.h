/**

 OmniFocusAnalyticsFramework
 
 <br/>
 
 Framework for pulling useful and interesting metrics out of an OmniFocus installation.
 
 <br/>
 
 Usage in README.md
 
 See LICENSE.txt for license information.
 
 @author <a href="mailto:ofaf@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
*/

#import <Foundation/Foundation.h>

#import "OFManager.h"
#import "OFTask.h"
#import "OFProject.h"
#import "OFContext.h"
#import "OFUtil.h"
#import "OFTaskFilter.h"
#import "OFProjectFilter.h"

@interface OmniFocusAnalyticsFramework : NSObject

@end
