// OFProjectFilter
// @see OFProjectFilter.h

#import "OFProjectFilter.h"

#import "OFProject.h"

@implementation OFProjectFilter
{
    /**
     Original projects provided via the initializer
     */
    NSArray *orig;
    /**
     OFManager provided via the initializer
     */
    OFManager *ofm;
}

#pragma mark - Initializer

-(id)initWithProjects:(NSArray *)list andManager:(OFManager *)manager
{
    self = [super init];
    if (self)
    {
        orig = list;
        ofm = manager;
    }
    return self;
}

#pragma mark - Filters

-(NSArray *)filterActiveProjects
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    NSEnumerator *pen = [orig objectEnumerator];
    OFProject *project;
    while (project = [pen nextObject])
    {
        if ([project active])
        {
            [ret addObject:project];
        }
    }
    
    return ret;
}

-(NSArray *)filterNonEmptyProjects
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];

    NSEnumerator *pen = [orig objectEnumerator];
    OFProject *project;
    while (project = [pen nextObject])
    {
        if ([[project tasks] count] > 0)
        {
            [ret addObject:project];
        }
    }
    
    return ret;
}

@end
