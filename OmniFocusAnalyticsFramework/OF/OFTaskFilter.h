/**
 
 OmniFocusAnalyticsFramework
 
 OFTaskFilter
 
 <br/>
 
 The task filter object is constructed with a list of tasks (and the OFManager) and 
 is used to spit out a list of tasks that match a specific filter parameter.
 
 <br/>
 
 General convention is filterX returns all tasks that match the condition X. i.e.,
 filterActiveTasks returns a list of all active tasks.
 
 <br/>
 
 This class is set to run on lists of objects, not unique ids.
 
 <br/>
 
 This class should make no modification to the provided list or objects.
 
 <br/>
 
 Usage in README.md
 
 @author <a href="mailto:ofaf@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>

#import "OFManager.h"

@interface OFTaskFilter : NSObject

/**
 Initialize the task filter with a list of tasks and the OFManager
 */
-(id)initWithTasks:(NSArray *)tasks andManager:(OFManager *)manager;

/**
 Return the list of active tasks
 */
-(NSArray *)filterActiveTasks;

/**
 Return the list of incomplete tasks
 */
-(NSArray *)filterIncompleteTasks;

/**
 Return the list of completed tasks
 */
-(NSArray *)filterCompletedTasks;

/**
 Return the list of non-repeating tasks
 */
-(NSArray *)filterNonRepeatingTasks;

/**
 Return the list of repeating tasks
 */
-(NSArray *)filterRepeatingTasks;

/**
 Return the list of tasks due within the next set number of minutes
 */
-(NSArray *)filterTasksDueWithinMinutes:(int)minutes;

/**
 Return the list of tasks completed within the previous set number of minutes
 */
-(NSArray *)filterTasksCompletedWithinMinutes:(int)minutes;

/**
 Return the list of tasks added within the previous set number of minutes
 */
-(NSArray *)filterTasksAddedWithinMinutes:(int)minutes;

/**
 Return the list of tasks that have the provided text in their notes field
 */
-(NSArray *)filterTasksMarkedWithTag:(NSString *)tag;

/**
 Return the list of flagged tasks
 */
-(NSArray *)filterFlaggedTasks;

/**
 Return the list of overdue tasks
 */
-(NSArray *)filterOverdueTasks;

/**
 Return the list of tasks that are in a context named by the provided string
 
 Implementation note - the way this method executes is by looking at the 
 task's [context fullyQualifiedContext] and doing a substring comparison.
 Therefore, passing in context "foo" will match the context fooBar, as well as
 foo and foo bar baz hierarchy
 */
-(NSArray *)filterInContextTasks:(NSString *)ctx;

/**
 Return the list of tasks that are not in a context named by the provided string
 
 @see notes from filterInContextTasks
 */
-(NSArray *)filterNotInContextTasks:(NSString *)ctx;

@end
