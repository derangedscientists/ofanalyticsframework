/**
 
 OmniFocusAnalyticsFramework
 
 OFTask
 
 <br/>
 
 The OFTask is the representation of a task object.
 
 <br/>
 
 Usage in README.md
 
 @author <a href="mailto:ofaf@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>
#import "OFManager.h"

@interface OFTask : NSObject

/**
 Initialize an OFTask with an OFManager and a dictionary of data. In the current modality, this data
 comes from a row of data from the OmniFocus database
 */
-(id)initWithManager:(OFManager *)mgr andData:(NSDictionary *)data;

/**
 Get the unique identifier of the task
 */
-(NSString *)key;

/**
 Get the unique id of the project that contains this task
 */
-(NSString *)project;

/**
 Get the human readable name of this task
 */
-(NSString *)name;

/**
 Get the unique id of the project that this task backs (@see OFProject for details)
*/
-(NSString *)projectLink;

/**
 Get the notes associated with this task
 */
-(NSString *)notes;

/**
 Get whether or not this task is flagged
 */
-(BOOL)flagged;

/**
 Get whether or not this task is complete
 */
-(BOOL)complete;

/**
 Get whether or not this task is repeating
 */
-(BOOL)repeating;

/**
 Get whether or not this task is overdue
 */
-(BOOL)overdue;

/**
 Get the unique id associated with this task's context
 */
-(NSString *)context;

/**
 Get the due date (in zulu time) of this task, or null if not set
 */
-(NSDate *)dateDue;

/**
 Get the completion date (in zulu time) of this task, or null if not set
 */
-(NSDate *)dateCompleted;

/**
 Get the added data (in zulu time) of this task
 */
-(NSDate *)dateAdded;

@end
