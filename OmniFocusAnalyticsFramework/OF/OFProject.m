// OFProject
// @see OFProject.h

#import "OFProject.h"

@implementation OFProject
{
    /**
     internal store of object data from the authoritative database
     */
    NSDictionary *data;
    
    /**
     the OFTask that provides the basis of this project
     */
    OFTask *backingTask;

    /**
     Children of this context, stored as unique ids that are dereferenced by the OFManager
     */
    NSArray *children;
    
    /**
     Reference to the associated manager
     */
    OFManager *manager;
}

#pragma mark - Table keys

/**
 Table key associated with the project's status
 */
+(NSString *)kStatus
{
    return @"status";
}

#pragma mark - Initiailizers

-(id)initWithManager:(OFManager *)mgr Data:(NSDictionary *)d andTask:(OFTask *)task
{
    self = [super init];
    if (self != nil)
    {
        children = [[NSArray alloc] init];
        data = d;
        manager = mgr;
        backingTask = task;
    }
    return self;
}

#pragma mark - Accessors

-(NSString *)key
{
    return backingTask.key;
}

-(NSString *)name
{
    return backingTask.name;
}

-(NSArray *)tasks
{
    return children;
}

-(void)addChild:(NSString *)key
{
    NSMutableArray *c = [children mutableCopy];
    [c addObject:key];
    children = c;
}

-(NSString *)status
{
    return [data objectForKey:[OFProject kStatus]];
}

-(BOOL)active
{
    return [[self status] isEqualToString:@"active"];
}

#pragma mark - Pretty printing

/**
 Pretty print the project
 */
-(NSString *)description
{
    return [[NSString alloc] initWithFormat:@"OFProject(key=%@, name=%@)", self.key, self.name];
}

@end
