/**
 
 OmniFocusAnalyticsFramework
 
 OFProject
 
 <br/>
 
 The OFProject is the representation of a project object.
 
 <br/>
 
 Implementation note - an OFProject's data is actually backed by an OFTask object that is decorated with
 additional information from the ProjectInfo table. Therefore, internally, data is pulled from one or both sources.
 
 <br/>
 
 Usage in README.md
 
 @author <a href="mailto:ofaf@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>

#import "OFManager.h"
#import "OFTask.h"

@interface OFProject : NSObject

/**
 Initialize an OFProject with an OFManager, dictionary of data, and root OFTask. In the current modality, this data
 comes from a row of data from the OmniFocus database
 */
-(id)initWithManager:(OFManager *)mgr Data:(NSDictionary *)data andTask:(OFTask *)task;

/**
 Get the unique identifier of the context
 */
-(NSString *)key;

/**
 Get the human readable name of this context
 */
-(NSString *)name;

/**
 Get an array of unique idenitifiers of the child tasks of this project
 */
-(NSArray *)tasks;

/**
 Add a child task to this project (using the child's unique identifier)
 */
-(void)addChild:(NSString *)key;

/**
 Boolean reresenting whether or not this task is active 
 */
-(BOOL)active;

/**
 NSString representing this project's status. Valid options include active, inactive, and done
 */
-(NSString *)status;

@end
