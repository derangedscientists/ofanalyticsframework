/**
 
 OmniFocusAnalyticsFramework
 
 OFUtil
 
 <br/>
 
 Utility methods associated with the OmniFocusAnalyticsFramework
 
 <br/>
 
 Usage in README.md
 
 @author <a href="mailto:ofaf@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>

@interface OFUtil : NSObject

/**
    Translate a raw date representation in the omnifocus database into an NSDate.
 
    Algorithm involves adding a magic number (978307200), which converts to seconds since epoch from seconds since 
    Jan 1, 2001 c.f. http://marc-abramowitz.com/archives/2011/03/08/accessing-omnifocus-data-as-a-sqlite-database/
 */
+(NSDate *)resolveDate:(double)date;

/**
    Get an NSDate representing the beginning (immediately after midnight) of today in zulu time
 */
+(NSDate *)beginningOfToday;

/**
    Get an NSDate representing the end (immediately before midnight) of today in zulu time
 */
+(NSDate *)endOfToday;

/**
    Calculate if a date is between two fence dates
 */
+(BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate;

@end
