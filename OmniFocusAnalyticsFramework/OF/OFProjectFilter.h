/**
 
 OmniFocusAnalyticsFramework
 
 OFProjectFilter
 
 <br/>
 
 The project filter object is constructed with a list of projects (and the OFManager) and
 is used to spit out a list of projects that match a specific filter parameter.
 
 <br/>
 
 General convention is filterX returns all projects that match the condition X. i.e.,
 filterActiveProjects returns a list of all active projects.
 
 <br/>
 
 This class is set to run on lists of objects, not unique ids.
 
 <br/>
 
 This class should make no modification to the provided list or objects.
 
 <br/>
 
 Usage in README.md
 
 @author <a href="mailto:ofaf@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>

#import "OFManager.h"

@interface OFProjectFilter : NSObject

/**
 Initialize the task filter with a list of tasks and the OFManager
 */
-(id)initWithProjects:(NSArray *)projects andManager:(OFManager *)manager;

/**
 Return list of active projects
 */
-(NSArray *)filterActiveProjects;

/**
 Return list of projects that have at least one task
 */
-(NSArray *)filterNonEmptyProjects;

@end
