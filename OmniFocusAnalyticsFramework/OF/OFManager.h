/**
 
 OmniFocusAnalyticsFramework
 
 OFManager
 
 <br/>
 
 The OFManager is responsible for providing access to OF objects in the world.
 
 <br/>
 
 Usage in README.md
  
 @author <a href="mailto:ofaf@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>

@interface OFManager : NSObject

#pragma mark - Properties

/** the location of the OF database */
@property NSString *database;

/** dictionary containing a mapping of unqiue id to all OFContext objects */
@property NSDictionary *contexts;
/** dictionary containing a mapping of unique id to all top level (i.e., no parent) OFContext objects */
@property NSDictionary *topLevelContexts;

/** dictionary containing a mapping of unqiue id to all OFProject objects */
@property NSDictionary *projects;
/** dictionary containing a mapping of unique id to all active OFProject objects */ // TODO - should probably go away in favor of the project filter
@property NSDictionary *activeProjects;

/** dictionary containing a mapping of unique id to all OFTask objects */
@property NSDictionary *tasks;

/** dictionary containing a mapping of unique id to all incomplete OFTask objects */ // TODO - should probably go away in favor of the task filter
@property NSDictionary *incompleteTasks;

#pragma mark - Initializer

/**
 Initialize with the path of the OmniFocus database 
 */
-(id)initWithDatabase:(NSString *)db;

#pragma mark - Accessors

/**
 Resolve a unique id into a OFTask, OFContext, or OFProject
 
 @return null if no object maps, otherwise an id pointing to an OFTask, OFContext, or OFProject
 */
-(id)lookup:(NSString *)key;

@end
