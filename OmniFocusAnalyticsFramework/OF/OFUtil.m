// OFUtil
// @see OFUtil.h

#import "OFUtil.h"

@implementation OFUtil

+(NSDate *)resolveDate:(double)date
{
    /* magic number, converts to seconds since epoch from seconds since Jan 1, 2001
     c.f. http://marc-abramowitz.com/archives/2011/03/08/accessing-omnifocus-data-as-a-sqlite-database/ */
    date += 978307200;
    NSDate *fdate = [NSDate dateWithTimeIntervalSince1970:date];

    return fdate;
}

+(NSDate *)beginningOfToday
{
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [cal setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: 0]];
    
    NSDateComponents * comp = [cal components:( NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[NSDate date]];
    
    [comp setMinute:0];
    [comp setHour:0];
    
    return [cal dateFromComponents:comp];
}

+(NSDate *)endOfToday
{
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [cal setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: 0]];
        
    NSDateComponents * comp = [cal components:( NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[NSDate date]];
        
    [comp setMinute:59];
    [comp setHour:23];
        
    return [cal dateFromComponents:comp];
}

+ (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    int offset = (int)(([[NSTimeZone localTimeZone] secondsFromGMT]) * -1);

    if ([date compare:beginDate] == NSOrderedAscending)
    {
        return NO;
    }
    
    if ([date compare:[endDate dateByAddingTimeInterval:offset]] == NSOrderedDescending)
    {
        return NO;
    }

    return YES;
}

@end
