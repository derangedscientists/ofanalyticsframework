/**
 
 OmniFocusAnalyticsFramework
 
 OFContext
 
 <br/>
 
 The OFContext is the representation of a context object.
 
 <br/>
 
 Usage in README.md
 
 @author <a href="mailto:ofaf@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>
#import "OFManager.h"

@interface OFContext : NSObject

/**
 Initialize an OFContext with an OFManager and a dictionary of data. In the current modality, this data
 comes from a row of data from the OmniFocus database
*/
-(id)initWithManager:(OFManager *)mgr andData:(NSDictionary *)data;

/**
 Get the unique identifier of the context
 */
-(NSString *)key;

/**
 Get the unique identifier of the parent of this context (or null if none exists)
 */
-(NSString *)parent;

/**
 Get the human readable name of this context
 */
-(NSString *)name;

/**
 Get an array of unique idenitifiers of the children of this context
 */
-(NSArray *)children;

/**
 Add a child of this context (using the child's unique identifier)
 */
-(void)addChild:(NSString *)key;

/**
 Pretty print the tree of child contexts using this context as the root
 */
-(NSString *)prettyPrintTree;

/**
 Return the fully qualified name of this context in the format of "Grandparent.name Parent.name self.name"
 */
-(NSString *)fullyQualifiedContext;

@end
