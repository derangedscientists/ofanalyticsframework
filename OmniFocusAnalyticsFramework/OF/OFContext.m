// OFContext
// @see OFContext.h

#import "OFContext.h"

@implementation OFContext
{
    /**
     internal store of object data from the authoritative database
     */
    NSDictionary *data;
    
    /**
     Children of this context, stored as unique ids that are dereferenced by the OFManager
     */
    NSArray *children;
    
    /**
     Reference to the associated manager
     */
    OFManager *manager;
}

#pragma mark - Table keys

/**
 Table key associated with the context's unique identifier 
 */
+(NSString *)kKey
{
    return @"persistentIdentifier";
}

/**
 Table key associated with the context's parent
 */
+(NSString *)kParent
{
    return @"parent";
}

/**
 Table key associated with the context's name
 */
+(NSString *)kName
{
    return @"name";
}

#pragma mark - Initiailizers

-(id)initWithManager:(OFManager *)mgr andData:(NSDictionary *)d
{
    self = [super init];
    if (self != nil)
    {
        children = [[NSArray alloc] init];
        data = d;
        manager = mgr;
    }
    return self;
}

#pragma mark - Accessors

-(NSString *)key
{
    return [data objectForKey:[OFContext kKey]];
}

-(NSString *)parent
{
    return [data objectForKey:[OFContext kParent]];
}

-(NSString *)name
{
    return [data objectForKey:[OFContext kName]];
}

-(NSArray *)children
{
    return children;
}

-(void)addChild:(NSString *)key
{
    NSMutableArray *c = [children mutableCopy];
    [c addObject:key];
    children = c;
}

#pragma mark - Pretty printing

/**
 Pretty print the context
 */
-(NSString *)description
{
    NSMutableString *buffer = [[NSMutableString alloc]
                               initWithFormat:@"OFContext(key=%@, name=%@, number of children=%lu)",
                               self.key, self.name, [[self children] count]];
    
    return buffer;
}

-(NSString *)prettyPrintTree
{
    NSMutableString *me = [[NSMutableString alloc] initWithFormat:@"OFContext(name=%@)", self.name];

    NSEnumerator *cen = [[self children] objectEnumerator];
    NSString *ckey;
    while (ckey = (NSString *)[cen nextObject])
    {
        OFContext *ctx = [manager lookup:ckey];
        [me appendFormat:@"\n-%@", [ctx prettyPrintTreeWithPrefix:@" "]];
    }
    return (NSString *)me;
}

/**
 Helper for pretty printing the tree, providing an intent prefix for multiple levels of indent
 */
-(NSString *)prettyPrintTreeWithPrefix:(NSString *)prefix
{
    NSMutableString *me = [[NSMutableString alloc] initWithFormat:@"OFContext(name=%@)", self.name];
    
    NSEnumerator *cen = [[self children] objectEnumerator];
    NSString *ckey;
    while (ckey = (NSString *)[cen nextObject])
    {
        OFContext *ctx = [manager lookup:ckey];
        NSMutableString *np = [[NSMutableString alloc] initWithFormat:@" %@", prefix];
        [me appendFormat:@"\n%@-%@", prefix, [ctx prettyPrintTreeWithPrefix:np]];
    }
    return (NSString *)me;
}

-(NSString *)fullyQualifiedContext
{
    NSMutableArray *stack = [[NSMutableArray alloc] init];
    NSString *pid;
    OFContext *pc = self;
    while ((pid = [pc parent]))
    {
        [stack addObject:pc];
        pc = [manager lookup:pid];
    }

    NSMutableString *buffer = [[NSMutableString alloc] init];
    for (int i=(int)([stack count] - 1); i>=0; i--)
    {
        OFContext *c = [stack objectAtIndex:i];
        [buffer appendFormat:@" %@ ", [c name]];
    }
    return buffer;
}

@end
