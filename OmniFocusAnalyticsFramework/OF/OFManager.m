// OFManager
// @see OFManager.h

/*
 
 Notes on implementation:
 
 Context and Task are straightforward in how they map from a table to an object
 
 Project is a combination of ProjectInfo and a Task. Both have unique ids, so lookup
 looks for both when doing a lookup. The actual OFProject object uses both tables.
 */

#import "OFManager.h"
#import "OFContext.h"
#import "OFTask.h"
#import "OFProject.h"

#import "../DB/FMDatabase.h"

@implementation OFManager
{
    /** 
     Cache to hold on the tasks that will be subsumed by a project
     */
    NSMutableDictionary *projectTaskCache;

    /**
     Mapping of unique id to a project info */
    NSMutableDictionary *project_info_map;
}

#pragma mark - synthesizers

@synthesize database;

@synthesize contexts;
@synthesize topLevelContexts;

@synthesize projects;
@synthesize activeProjects;

@synthesize tasks;
@synthesize incompleteTasks;

#pragma mark - initializer

-(id)initWithDatabase:(NSString *)db
{
    NSLog(@"Start initializing OFManager");
    self = [super init];
    
    if (self != nil)
    {
        self.database = db;
        self.contexts = [[NSDictionary alloc] init];
        self.topLevelContexts = [[NSDictionary alloc] init];
        self.projects = [[NSDictionary alloc] init];
        self.activeProjects = [[NSDictionary alloc] init];
        self.tasks = [[NSDictionary alloc] init];
        self.incompleteTasks = [[NSDictionary alloc] init];
        projectTaskCache = [[NSMutableDictionary alloc] init];
        project_info_map = [[NSMutableDictionary alloc] init];
        [self populateWithDB:self.database];
    }
    
    NSLog(@"Done initializing OFManager");
    return self;
}

#pragma mark - description

-(NSString *)description
{
    return [[NSString alloc] initWithFormat:@"OFManager(db=%@)", self.database];
}

#pragma mark - lookup

-(id)lookup:(NSString *)key
{
    id candidate = [[self contexts] objectForKey:key];
    if (candidate != nil) {
            return candidate;
        }
        
    candidate = [[self projects] objectForKey:key];
    if (candidate != nil) {
        return candidate;
    }
        
    candidate = [[self tasks] objectForKey:key];
    if (candidate != nil) {
        return candidate;
    }
    
    candidate = [project_info_map objectForKey:key];
    if (candidate != nil) {
        return candidate;
    }
    
    return nil;
}

#pragma mark - Populate methods

/**
 Main driver to populate the internal cache of objects from the database
*/
-(void)populateWithDB:(NSString *)db
{
    NSLog(@"Populating from FMDatabase.");
    FMDatabase *fdb = [[FMDatabase alloc] initWithPath:db];
    [fdb open];
    
    FMResultSet *rs;
    
    NSString *contextQuery = @"SELECT * from CONTEXT";
    rs = [fdb executeQuery:contextQuery];
    [self populateContextsWithResultSets:rs];
    
    NSString *taskQuery = @"SELECT * from TASK";
    rs = [fdb executeQuery:taskQuery];
    [self populateTasksWithResultSets:rs];
    
    NSString *projectQuery = @"SELECT * from ProjectInfo";
    rs = [fdb executeQuery:projectQuery];
    [self populateProjectsWithResultSets:rs];
        
    [fdb close];
    NSLog(@"Done populating from FMDatabase");
}

/*
 Populate contexts from the result set from populateWithDB
*/
-(void)populateContextsWithResultSets:(FMResultSet *)rs
{
    NSMutableDictionary *ctxs = [contexts mutableCopy];
    while([rs next]) {
        NSDictionary *values = [rs resultDictionary];
        OFContext *ofc = [[OFContext alloc] initWithManager:self andData:values];
        [ctxs setObject:ofc forKey:[ofc key]];
    }
    self.contexts = ctxs;
    
    [self buildContextTree];
}

/*
 Populate tasks from the result set from populateWithDB
 
 Siphons off those tasks that are actually projects, based on whether or not it
 has a projectLink field.
 */
-(void)populateTasksWithResultSets:(FMResultSet *)rs
{
    NSMutableDictionary *tsks = [tasks mutableCopy];
    NSMutableDictionary *incomplete = [incompleteTasks mutableCopy];
    while([rs next]) {
        NSDictionary *values = [rs resultDictionary];
        OFTask *oft = [[OFTask alloc] initWithManager:self andData:values];
        if (oft.projectLink == (NSString *)[NSNull null])
        {
            [tsks setObject:oft forKey:[oft key]];
        } else
        {
            [projectTaskCache setObject:oft forKey:oft.key];
        }
        
        if (oft.complete == FALSE) {
            [incomplete setObject:oft forKey:oft.key];
        }
    }
    self.tasks = tsks;
    self.incompleteTasks = incomplete;
}

/*
 Populate projects from the result set from populateWithDB
 
 Consumes tasks that were set aside in populateTasksWithResultSets
 
 */
-(void)populateProjectsWithResultSets:(FMResultSet *)rs
{
    NSMutableDictionary *projs = [projects mutableCopy];
    NSMutableDictionary *ap = [activeProjects mutableCopy];
    
    while([rs next]) {
        NSDictionary *values = [rs resultDictionary];
        NSString *tid = [values objectForKey:@"task"];
        OFTask *bt = [projectTaskCache objectForKey:tid];
        OFProject *ofp = [[OFProject alloc] initWithManager:self Data:values andTask:bt];
        [projs setObject:ofp forKey:[ofp key]];
        [project_info_map setObject:ofp forKey:[values objectForKey:@"pk"]];
        if ([ofp active])
        {
            [ap setObject:ofp forKey:[ofp key]];
        }
    }
    self.projects = projs;
    self.activeProjects = ap;
    [self buildProjectTree];
}

#pragma mark - helper methods

/**
 Build the context hierarchy, linking contexts with their child contexts
 */
-(void)buildContextTree
{
    NSDictionary *ctx = [self.contexts copy];
    NSEnumerator *cen = [ctx keyEnumerator];
    
    NSString *key;
    while (key = [cen nextObject])
    {
        OFContext *context = (OFContext *)[self lookup:key];
        if (context.parent != (NSString *)[NSNull null])
        {
            NSString *pkey = context.parent;
            OFContext *pcontext = (OFContext *)[self lookup:pkey];
            [pcontext addChild:key];
        } else
        {
            NSMutableDictionary *tlc = [self.topLevelContexts mutableCopy];
            [tlc setObject:context forKey:context.key];
            self.topLevelContexts = tlc;
        }
    }
}

/**
 Build the project hierarchy, linking projects with their child projects
 */
-(void)buildProjectTree
{
    NSDictionary *tsks = [self.tasks copy];
    NSEnumerator *cen = [tsks keyEnumerator];
    
    NSString *key;
    while (key = [cen nextObject])
    {
        OFTask *task = (OFTask *)[self lookup:key];
        NSString *pkey = [task project];
        OFProject *project = [self lookup:pkey];
        [project addChild:task.key];
    }    
}

@end
