// OFTask
// @see OFTask.h

#import "OFTask.h"
#import "OFUtil.h"

@implementation OFTask
{
    
    /**
     internal store of object data from the authoritative database
     */
    NSDictionary *data;
    
    /**
     Reference to the associated manager
     */
    OFManager *manager;
}

#pragma mark - Table keys

/**
    Table key for the context
 */
+(NSString *)kContext
{
    return @"context";
}

/**
    Table key for getting repetition method
 */
+(NSString *)kRepetitionMethod
{
    return @"repetitionMethodString";
}

/**
    Table key for getting the link to a project, determining 
 whether or not this task is backing a project
 */
+(NSString *)kProjectLink
{
    return @"projectInfo";
}

/**
    Table key for getting the flagged status
 */
+(NSString *)kFlagged
{
    return @"flagged";
}

/**
    Table key for getting this task's unique identifier
 */
+(NSString *)kKey
{
    return @"persistentIdentifier";
}

/**
    Table key for getting this task's parent project
 */
+(NSString *)kProject
{
    return @"containingProjectInfo";
}

/**
    Table key for getting the human readable name of this task
 */
+(NSString *)kName
{
    return @"name";
}

/**
    Table key for getting this task's note
 */
+(NSString *)kNote
{
    return @"noteXMLData";
}

/**
 Table key for getting the completion date of this task
 */
+(NSString *)kDateCompleted
{
    return @"dateCompleted";
}

/**
 Table key for getting the due date of this task
 */
+(NSString *)kDateDue
{
    return @"dateDue";
}

/**
 Table key for getting the added date of this task
 */
+(NSString *)kDateAdded
{
    return @"dateAdded";
}

/**
 Table key for getting the overdue status of this task
 */
+(NSString *)kOverdue
{
    return @"isOverdue";
}

#pragma mark - Initializer

-(id)initWithManager:(OFManager *)mgr andData:(NSDictionary *)d
{
    self = [super init];
    if (self != nil)
    {
        data = d;
        manager = mgr;
    }
    return self;
}

#pragma mark - Accessors

-(NSString *)key
{
    return [data objectForKey:[OFTask kKey]];
}

-(NSString *)project
{
    return [data objectForKey:[OFTask kProject]];
}

-(NSString *)name
{
    return [data objectForKey:[OFTask kName]];
}

-(NSString *)projectLink
{
    return [data objectForKey:[OFTask kProjectLink]];
}

-(NSString *)notes;
{
    NSData *d = [data objectForKey:[OFTask kNote]];
    NSString *rdata;
    if (d != (NSData *)[NSNull null])
    {
        rdata = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
    }
    return rdata;
}

-(BOOL)flagged
{
    NSNumber *flag = [data objectForKey:[OFTask kFlagged]];
    return [flag intValue] == 1;
}

-(BOOL)complete
{
    NSNumber *date = [data objectForKey:[OFTask kDateCompleted]];
    return !(date == (NSNumber *)[NSNull null]);
}

-(BOOL)repeating
{
    NSString *rep = [data objectForKey:[OFTask kRepetitionMethod]];
    return rep != (NSString *)[NSNull null];
}

-(NSString *)context
{
    return [data objectForKey:[OFTask kContext]];
}

-(NSDate *)dateCompleted
{
    NSNumber *raw = [data objectForKey:[OFTask kDateCompleted]];
    return [OFUtil resolveDate:[raw doubleValue]];
}

-(NSDate *)dateDue
{
    NSNumber *raw = [data objectForKey:[OFTask kDateDue]];
    if (raw != (NSNumber *)[NSNull null])
    {
        return [OFUtil resolveDate:[raw doubleValue]];
    }
    return nil;
}

-(NSDate *)dateAdded
{
    NSNumber *raw = [data objectForKey:[OFTask kDateAdded]];
    if (raw != (NSNumber *)[NSNull null])
    {
        return [OFUtil resolveDate:[raw doubleValue]];
    }
    return nil;
}

-(BOOL)overdue
{
    NSNumber *flag = [data objectForKey:[OFTask kOverdue]];
    return [flag intValue] == 1;    
}

#pragma mark - Pretty print

-(NSString *)description
{
    return [[NSString alloc] initWithFormat:@"OFTask(name=%@)", [self name]];
}

@end
