// OFTaskFilter
// @see OFTaskFilter.h

#import "OFTaskFilter.h"

#import "OFProject.h"
#import "OFUtil.h"
#import "OFContext.h"

@implementation OFTaskFilter
{
    /**
     Original tasks provided via the initializer
     */
    NSArray *orig;
    /**
     OFManager provided via the initializer
     */
    OFManager *ofm;
}

#pragma mark - Initializer

-(id)initWithTasks:(NSArray *)list andManager:(OFManager *)manager
{
    self = [super init];
    if (self)
    {
        orig = list;
        ofm = manager;
    }
    return self;
}

#pragma mark - Filters

-(NSArray *)filterActiveTasks
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        NSString *pid = [task project];
        OFProject *project = [ofm lookup:pid];
        if ([project active])
        {
            [ret addObject:task];
        }
    }
    return ret;
}

-(NSArray *)filterIncompleteTasks
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        if (![task complete])
        {
            [ret addObject:task];
        }
    }
    return ret;
}

-(NSArray *)filterCompletedTasks
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        if ([task complete])
        {
            [ret addObject:task];
        }
    }
    return ret;
}

-(NSArray *)filterNonRepeatingTasks
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        if (![task repeating])
        {
            [ret addObject:task];
        }
    }
    return ret;
}

-(NSArray *)filterRepeatingTasks
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        if ([task repeating])
        {
            [ret addObject:task];
        }
    }
    return ret;}

-(NSArray *)filterTasksDueWithinMinutes:(int)minutes
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        if ([task dateDue] != nil)
        {
            NSDate *now = [NSDate date];
            NSDate *interval = [now dateByAddingTimeInterval:minutes];
            if ([OFUtil date:[task dateDue] isBetweenDate:now andDate:interval])
            {
                [ret addObject:task];
            }
        }
    }
    
    return ret;
}

-(NSArray *)filterTasksCompletedWithinMinutes:(int)minutes
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        if ([task dateCompleted] != nil)
        {
            NSDate *now = [NSDate date];
            NSDate *interval = [now dateByAddingTimeInterval:(-1*minutes)];
            if ([OFUtil date:[task dateCompleted] isBetweenDate:interval andDate:now])
            {
                [ret addObject:task];
            }
        }
    }
    
    return ret;
}

-(NSArray *)filterTasksAddedWithinMinutes:(int)minutes
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        if ([task dateAdded] != nil)
        {
            NSDate *now = [NSDate date];
            NSDate *interval = [now dateByAddingTimeInterval:(-1*minutes)];
            if ([OFUtil date:[task dateAdded] isBetweenDate:interval andDate:now])
            {
                [ret addObject:task];
            }
        }
    }
    
    return ret;
}

-(NSArray *)filterTasksMarkedWithTag:(NSString *)tag
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];

    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        NSString *notes = [task notes];
        if (notes != nil)
        {
            if ([notes rangeOfString:tag].location != NSNotFound)
            {
                [ret addObject:task];
            }
        }
    }
    
    return ret;
}

-(NSArray *)filterFlaggedTasks
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        if ([task flagged])
        {
            [ret addObject:task];
        }
    }
    
    return ret;
}

-(NSArray *)filterOverdueTasks
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        if ([task overdue])
        {
            [ret addObject:task];
        }
    }
    
    return ret;
}

-(NSArray *)filterInContextTasks:(NSString *)tctx
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        NSString *cid = [task context];
        OFContext *ctx = [ofm lookup:cid];
        
        NSString *fqc = [ctx fullyQualifiedContext];
        if (fqc != nil)
        {
            if ([fqc rangeOfString:tctx].location != NSNotFound)
            {
                [ret addObject:task];
            }
        }
        
    }
    
    return ret;
}

-(NSArray *)filterNotInContextTasks:(NSString *)tctx
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    NSEnumerator *ten = [orig objectEnumerator];
    OFTask *task;
    while (task = [ten nextObject])
    {
        NSString *cid = [task context];
        OFContext *ctx = [ofm lookup:cid];
        
        NSString *fqc = [ctx fullyQualifiedContext];
        if (fqc != nil)
        {
            if ([fqc rangeOfString:tctx].location == NSNotFound)
            {
                [ret addObject:task];
            }
        }
        
    }
    
    return ret;
}

@end
